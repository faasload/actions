# Main Makefile
#
# The target "all" will build all actions of all runtimes, i.e., all subfolders of subfolders in `src`. It delegates
# building the actions of a runtime to a specific Makefile found in the runtime subfolder.
#
# The last step is to gather the built actions by copying the archives built by the specific Makefiles to the directory
# "OUTPUT_DIR".
#
# Call as:
# * make, make all
# * make python: make all Python actions
# * make python/wand: make all Python Wand actions
# * make python/wand/blur: make Python Wand action "blur"

# Source directory
SRC = src
# Build output directory
BUILD = build
# Path to executable "wskdeploy"
WSKDEPLOY = wskdeploy


# c java11 python...
LANGUAGES := $(patsubst $(SRC)/%/.,%,$(wildcard $(SRC)/[^_]*/.))
# c/none java11/imagej java11/none python/wand python/pydub...
DEPENDENCIES := $(patsubst $(SRC)/%/.,%,$(foreach lang,$(LANGUAGES),$(wildcard $(SRC)/$(lang)/[^_]*/.)))
# c/none/malloc-test java11/imagej/blur python/wand/resize...
ACTIONS := $(patsubst $(SRC)/%/.,%,$(foreach dep,$(DEPENDENCIES),$(wildcard $(SRC)/$(dep)/[^_]*/.)))

LIST_DEPENDENCIES := $(LANGUAGES:%=list-%-dependencies)
LIST_ACTIONS := $(DEPENDENCIES:%=list-%-actions)

CLEAN_LANGUAGES := $(addprefix clean-,$(LANGUAGES)) 
CLEAN_DEPENDENCIES := $(addprefix clean-,$(DEPENDENCIES)) 
CLEAN_ACTIONS := $(addprefix clean-,$(ACTIONS)) 

include display.mk

.PHONY: all help list-languages $(LIST_DEPENDENCIES) $(LIST_ACTIONS) $(LANGUAGES) $(DEPENDENCIES) $(ACTIONS) clean $(CLEAN_DEPENDENCIES) $(CLEAN_ACTIONS) deploy

all: $(LANGUAGES)

help:
	@cat README.Makefile.md

list-languages:
	@echo "Action languages:"
	@echo
	@for lang in $(LANGUAGES); do \
		echo "* $$lang"; \
	done

# Add `|| true` to "condition" to have a successful return code anyway.
$(LIST_DEPENDENCIES): list-%-dependencies:
	@echo "Dependencies of $* actions:"
	@echo
	@for dep in $(DEPENDENCIES); do \
		[[ "$$dep" =~ ^"$*"/.+$$ ]] && echo "* $$(basename "$$dep")" || true; \
	done

# Add `|| true` to "condition" to have a successful return code anyway.
$(LIST_ACTIONS): list-%-actions:
	@echo "$(*D) $(*F) actions:"
	@echo
	@for act in $(ACTIONS); do \
		[[ "$$act" =~ ^"$(*D)/$(*F)"/.+$$ ]] && echo "* $$(basename "$$act")" || true; \
	done

# Normal case: actions are packaged as Zip archives.
$(filter-out java/% java11/%,$(ACTIONS)): %: $(BUILD)/%.zip

# Java case: actions are packaged as Jar archives.
$(filter java/% java11/%,$(ACTIONS)): %: $(BUILD)/%.jar

$(BUILD)/%:
	$(call echo_info,making $$(dirname "$(*D)") $(notdir $(*D)) action $(basename $(*F)) ($(@F)))
	$(MAKE) --directory "$(SRC)/$(*D)" $(@F)
	mkdir --parents "$(@D)"
	ln --symbolic "../../../$(SRC)/$(*D)/$(@F)" "$@"

clean: $(CLEAN_LANGUAGES)
	rm --dir --force "$(BUILD)"

$(CLEAN_DEPENDENCIES): clean-%:
	$(call echo_info,cleaning all $(*D) $(*F) actions)
	$(MAKE) --directory "$(SRC)/$*" clean
	rm --force --recursive "$(BUILD)/$*"

$(CLEAN_ACTIONS): clean-%:
	$(call echo_info,cleaning $$(dirname "$(*D)") $(notdir $(*D)) action $(basename $(*F)))
	$(MAKE) --directory "$(SRC)/$(*D)" "clean-$(*F)"
	rm --force "$(BUILD)/$*".*

deploy: manifest.yml
	$(call echo_info,deploying all actions to OpenWhisk)
	$(WSKDEPLOY) --param-file swift_parameters.json

# Second expansion is needed to filter prerequisites (dependencies or actions) using the target, i.e., **after** the
# rule has been read by make.
.SECONDEXPANSION:

$(LANGUAGES): $$(filter $$@/%,$$(DEPENDENCIES))
	$(call echo_info,built all $@ actions)

$(DEPENDENCIES): $$(filter $$@/%,$$(ACTIONS))
	$(call echo_info,built all $(@D) $(@F) actions)

# Escape percent sign.
PERCENT = %
$(CLEAN_LANGUAGES): clean-%: $$(filter clean-$$*/$$(PERCENT),$$(CLEAN_DEPENDENCIES))
	rm --dir --force "$(BUILD)/$*"

