# Display routines.
#
# Included in main Makefile, for use in all.

ifeq (0, $(shell tput colors >/dev/null 2>&1; echo $$?))
export reset := $(shell tput sgr0)

export bold := $(shell tput bold)
# tput does not have a default capability to turn bold off, but most often
# it is 22.
export normal := \e[22m
export underline := $(shell tput smul)
export nounderline := $(shell tput rmul)

export red := $(shell tput setaf 1)
export green := $(shell tput setaf 2)
export yellow := $(shell tput setaf 3)
export blue := $(shell tput setaf 4)
endif

export echo_info = @echo "$(blue)$(1)$(reset)"
export echo_warn = @echo "$(yellow)$(1)$(reset)"
