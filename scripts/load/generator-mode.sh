#!/usr/bin/env bash

# Load sample inputs for each input kind.
# This script is mostly used for generator mode of FaaSLoad.

SWIFT_PARAMS="../../swift_parameters.json"
SAMPLES_DIR="../../samples"
ALL_INPUT_KINDS="image audio video"

if [[ "$1" =~ ^-.*$ ]]; then
    echo "Usage: $0 [N [KINDS...]]"
    echo
    echo "* N: number of samples to load (default: all samples)"
    echo "* KINDS: kinds of inputs to load (default: all kinds)"
    echo
    echo "Input kinds are:"
    echo
    for kind in $ALL_INPUT_KINDS; do
        echo "* $kind"
    done
    echo
    echo "Create one Swift container per input kind, and fill it with samples of matching kind."

    exit 2
fi >&2


samples="$1"
if [ -z "$samples" ]; then
    samples=all
fi
shift
kinds="$*"
if [ -z "$kinds" ]; then
    kinds=$ALL_INPUT_KINDS
fi


if ! which "jq" > /dev/null 2>&1 || ! which "swift" > /dev/null 2>&1; then
    echo "swift and jq are required in your \$PATH." >&2

    exit 2
fi


swift_user="$(jq -r ".user" "$SWIFT_PARAMS")"
swift_key="$(jq -r ".key" "$SWIFT_PARAMS")"
swift_authurl="$(jq -r ".authurl" "$SWIFT_PARAMS")"


for kind in $kinds; do
    input_dir="$SAMPLES_DIR/$kind"

    echo "Uploading $samples $kind inputs"

    swift_dir="$kind"

    # Notice the inputs are not resolved here, the glob is left literal.
    # It will be resolved in the following for loop.
    if [ "$samples" = all ]; then
        inputs="$input_dir/*"
    else
        inputs="$(seq 1 "$samples" | xargs -I"{}" echo "$input_dir/{}.*")"
    fi

    i=1
    for input in $inputs; do
        input_ext="${input##*.}"

        echo -n "$i "

        swift --quiet\
            --auth "$swift_authurl" --user "$swift_user" --key "$swift_key"\
            upload --object-name "$i.$input_ext" "$swift_dir" "$input"

        i=$(expr $i + 1)
    done

    echo
done

