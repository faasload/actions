# Scripts to load sample inputs

* [`generator-mode.sh`](generator-mode.sh): load sample inputs for FaaSLoad's generator mode
* [`injector-mode.sh`](injector-mode.sh): load sample inputs for FaaSLoad's injector mode
* [`same-input.sh`](same-input.sh): similar to `generator-mode.sh`, but always with the same sample input per input kind

Run them with `--help` as first argument to see their detailed help and usage.

Requirements:

* [Swift client](https://pypi.org/project/python-swiftclient/) (from PyPI or packaged by your distribution)
* [jq](https://jqlang.github.io/jq/)
