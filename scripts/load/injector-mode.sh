#!/usr/bin/env bash

# Load sample inputs for users of injection traces.
#
# This script is mostly used for injector mode of FaaSLoad.
# It finds the users from the injection traces.
# However this uploads all sample inputs of all input kinds, without regard for the actual inputs listed in the traces.

SWIFT_PARAMS="../../swift_parameters.json"
SAMPLES_DIR="../../samples"
INPUT_KINDS="image audio video"


if [ -z "$1" ] || [[ "$1" =~ ^-.*$ ]]; then
    echo "Usage: $0 TRACE_DIR"
    echo
    echo "* TRACE_DIR: directory of injection traces, as produced by the trace builder"
    echo
    echo "Create one Swift container per user in the injector traces, and fill it with all samples of all input kinds."

    exit 2
fi >&2


if ! which "jq" > /dev/null 2>&1 || ! which "swift" > /dev/null 2>&1; then
    echo "swift and jq are required in your \$PATH." >&2

    exit 2
fi


trace_dir="$1"


swift_user="$(jq -r ".user" "$SWIFT_PARAMS")"
swift_key="$(jq -r ".key" "$SWIFT_PARAMS")"
swift_authurl="$(jq -r ".authurl" "$SWIFT_PARAMS")"


users="$(find "$trace_dir" -type f -name "*.faas" -exec head -n1 "{}" ";"\
         | cut -f1 | sort | uniq)"
if [ -z "$users" ]; then
    echo "ERROR: no users found from traces (no traces in \"$trace_dir\", or malformed headers?)" >&2

    exit 1
fi

for user in $users; do
    echo "Uploading inputs for $user"

    swift_dir="$user"

    for kind in $INPUT_KINDS; do
        input_dir="$SAMPLES_DIR/$kind"

        echo "Uploading all $kind inputs"

        i=1
        for input in "$input_dir/"*; do
            input_ext="${input##*.}"

            echo -n "$i "

            swift --quiet\
                --auth "$swift_authurl" --user "$swift_user" --key "$swift_key"\
                upload --object-name "$i.$input_ext" "$swift_dir" "$input"

            i=$(expr $i + 1)
        done

        echo
    done
done
