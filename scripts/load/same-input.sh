#!/usr/bin/env bash

# Load multiple copies of the **same** sample input for each input kind.
# This script is mostly used for generator mode of FaaSLoad, to just generate a dataset of runs of the same action under
# the same input.

SWIFT_PARAMS="../../swift_parameters.json"
SAMPLES_DIR="../../samples"
ALL_INPUT_KINDS="image audio video"


if [ -z "$1" ] || [[ "$1" =~ ^-.*$ ]]; then
    echo "Usage: $0 N [KINDS...]"
    echo
    echo "* N: number of copies of the first sample to load"
    echo "* KINDS: kinds of inputs to load (default: all kinds)"
    echo
    echo "Input kinds are:"
    echo
    for kind in $ALL_INPUT_KINDS; do
        echo "* $kind"
    done
    echo
    echo "Create one Swift container per input kind, and fill it with copies of the same sample input of matching kind."

    exit 2
fi >&2


copies="$1"
shift
kinds="$*"
if [ -z "$kinds" ]; then
    kinds=$ALL_INPUT_KINDS
fi


if ! which "jq" > /dev/null 2>&1 || ! which "swift" > /dev/null 2>&1; then
    echo "swift (the Swift storage client), and jq are required in your \$PATH." >&2

    exit 2
fi


swift_user="$(jq -r ".user" "$SWIFT_PARAMS")"
swift_key="$(jq -r ".key" "$SWIFT_PARAMS")"
swift_authurl="$(jq -r ".authurl" "$SWIFT_PARAMS")"


for kind in $kinds; do
    input_dir="$SAMPLES_DIR/$kind"

    echo "Uploading $copies $kind inputs"

    swift_dir="$kind"
    # Assuming there is only one input with filename "1" with any extension;
    # and that it does have an extension.
    input="$(echo "$input_dir"/1.*)"
    input_ext="${input##*.}"

    for i in $(seq 1 "$copies"); do
        echo -n "$i "

        swift --quiet\
            --auth "$swift_authurl" --user "$swift_user" --key "$swift_key"\
            upload --object-name "$i.$input_ext" "$swift_dir" "$input"
    done

    echo
done

