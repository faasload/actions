# OpenWhisk actions for FaaSLoad

Actions (cloud functions) for Apache OpenWhisk, to use with [FaaSLoad](https://gitlab.com/faasload/faasload).

The actions are prepared to be used with FaaSLoad in generator and injector modes.
It means they use the expected set of parameters and are written to use a Swift storage backend for their inputs.
Nonetheless, they can be used without FaaSLoad, they are plain OpenWhisk actions.

## Quick start

Before using the actions as instructed in FaaSLoad's documentation, you must prepare them as follows:

1. build the actions
2. set up the Swift storage

### Build the actions

Run `make` in the main folder, assuming you have installed the build dependencies.

The build dependencies are:

* Docker: actions must be built with the same environments, i.e., Docker images, they will run in, also some actions require building custom Docker images
* Zip: to make archives with the actions' artifacts

### Setup the Swift storage

In short:

1. run a Swift storage instance
2. set the info of the Swift storage instance in [`swift_parameters.json`](swift_parameters.json)
3. load inputs

#### Run a Swift storage instance

You can use any Swift storage instance that will be accessible to your actions.

If you just want an easy local deployment, you can run the Swift backend storage as a Docker container using the image [fnndsc/docker-swift-onlyone](https://hub.docker.com/r/fnndsc/docker-swift-onlyone):

```sh
docker run -d --name swift-onlyone -e SWIFT_USERNAME=test:tester -e SWIFT_KEY=testing -p 12345:8080 -v swift_storage:/srv -t fnndsc/docker-swift-onlyone
```

* authentication URL: `http://127.0.0.1:12345/auth/v1.0`
* user: `test:tester`
* key: `testing`

So for example, to list the content of the storage:

```sh
swift -A http://127.0.0.1:12345/auth/v1.0 -U test:tester -K testing list
```

#### Set the info of the Swift storage

[`swift_parameters.json`](swift_parameters.json) is used to set default values to a few parameters of the actions related to the Swift storage.
The load scripts in [`scripts/`](scripts/) read this file to load the sample inputs of the actions;
FaaSLoad's setup scripts also read it when preparing OpenWhisk for a run (in details, it is used as a [parameter file to set default parameters on the package of the actions](https://github.com/apache/openwhisk/blob/master/docs/parameters.md#working-with-parameter-files).

Make sure the IP address and port of the Swift storage you write in [`swift_parameters.json`](swift_parameters.json) are accessible from your actions.
In particular, you probably cannot use `http://127.0.0.1:12345` as shown in examples above when running a single-container deployment of the Swift storage.

#### Load inputs

You must fill the Swift storage with the inputs that your actions will require when run with FaaSLoad.

You can use sample inputs provided in this repository via Git LFS under [`samples/`](samples/), stored in folders matching their kind (image, video, etc.).

First, pull the samples with Git LFS:

```sh
git lfs pull
```

Depending on your use case, you have different scripts in [`scripts/`](scripts/) (they can also serve as inspirations to how to load custom inputs).
See their README in [`scripts/`](scripts/).

## Details

Here are some details on using actions in this repository.

### Building actions

In general, simply run `make`, possibly targeting only one action.

Actions are built in the `build` directory as Zip archives (or Jar archives for Java actions), whether they require dependencies or not;
in addition, a few actions (such as the ones in `src/py/wand` which are based on the Python library Wand, of which ImageMagick is a native dependency) require a special Docker runtime image, which is built and added to the local Docker image repository.
In particular, most actions extract data from, and load data to a Swift storage backend, and as such, they have a dependency on a Swift client library in the language they are written in.

This is all recorded in READMEs in the language directories;
see [`src/`](src/) for more details on the actions themselves.

### Deploying actions in OpenWhisk

This repository does not deal with deploying actions in OpenWhisk, this is done with FaaSLoad's setup scripts.
However, they are declared here, in a Manifest file [`manifest.yml`](manifest.yml), that is read by FaaSLoad's tooling.
This is actually an OpenWhisk feature, to be used with its tool [wskdeploy](https://github.com/apache/openwhisk-wskdeploy), so you may also use the actions without FaaSLoad and deploy them all at once with `make deploy` (provided you have wskdeploy in your `$PATH`).

If you want to deploy one specific action manually, you can execute `wsk action create [...]`.
The actual command line depends on the nature of the action (it depends on its dependency, actually);
see [`src/`](src/) for specifics.
