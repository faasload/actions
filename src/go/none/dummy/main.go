package main

import (
    "fmt"
    "log"
    "strconv"
    "time"
)

func Main(args map[string]interface{}) map[string]interface{} {
    waitTime := 0.0
    allocatedMemory := 0.0

    msg := make(map[string]interface{})

    waitTime_arg, present := args["toWait"]
    if !present {
        msg["error"] = "missing parameter toWait"
        return msg
    }
    switch waitTime_arg.(type) {
    default:
        msg["error"] = "unexpected type for parameter toWait"
        return msg
    case string:
        waitTime_conv, err := strconv.ParseFloat(waitTime_arg.(string), 64)
        if err != nil {
            msg["error"] = "failed converting parameter toWait to float"
            return msg
        }
        waitTime = waitTime_conv
    case float64:
        waitTime = waitTime_arg.(float64)
    }

    log.Printf("waitTime = %s\n", waitTime)

    allocatedMemory_arg, present := args["toAllocate"]
    switch allocatedMemory_arg.(type) {
    default:
        msg["error"] = "unexpected type for parameter toAllocate"
        return msg
    case string:
        allocatedMemory_conv, err := strconv.ParseFloat(allocatedMemory_arg.(string), 64)
        if err != nil {
            msg["error"] = "failed converting parameter toAllocate to float"
            return msg
        }
        allocatedMemory = allocatedMemory_conv
    case float64:
        allocatedMemory = allocatedMemory_arg.(float64)
    }

    log.Printf("allocatedMemory = %s\n", allocatedMemory)

    slice := make([]byte, int(1024*allocatedMemory))

    time.Sleep(time.Duration(waitTime) * time.Millisecond)

    msg["body"] = fmt.Sprintf("Waited: %.3fs - Allocated memory: %.3fMB", waitTime/1000, float64(len(slice))/(1024*1024))

    return msg
}
