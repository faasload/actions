# Go actions: none
 
Actions written in Go, that do not use any major dependency;
they may still use minor dependencies (i.e., that do not define their features), including the Swift storage client.

* [`dummy`](dummy/): dummy actions that mimics execution time and memory usage

## Building

Currently, those Go actions do not require any dependency;
see [README for Go actions](../README.md).

## Deploying

Those Go actions only require that their kind be specified (as `go:1.18`), because even without any dependency, they are packaged as Zip archives;
See [README for Go actions](../README.md) for full instructions.
