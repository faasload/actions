# Go actions

| Dependency | Input kind | Dependency package | Runtime image | Image content |
|-|-:|-:|-:|-:|
| [`none`](none/) | params | no | no | N/A |

## Building

Here are general instructions to build Go actions.
They also cover actions that require native dependencies;
see dependencies' specific READMEs for special requirements.

General variables used in code examples below:

```sh
# Name (and directory) of the action to build.
action=myaction
# Name (and directory) of the main dependency of the action (i.e., where the action is located).
dependency=thedependency
# Custom Docker runtime image for the action.
docker=action-golang-v1.18:mycustomtag
```

Go actions are simply built from their source, as Go is a compiled language.

Note that it is possible with the OpenWhisk runtime to simple send uncompiled Go code, and it will be compiled on-the-fly before invoking the action.
Here, we always compile the actions before deploying them.

```sh
# Run from the dependency's directory.
cd $dependency
docker run -i openwhisk/action-golang-v1.18 -compile main < ./$action/main.go > $action.zip
```

## Deploying

See dependencies' specific READMEs for special requirements.
Below are generic instructions.

To create a Go action, the kind must be specified because we use a Zip file which hides the nature of the action's source code.

```sh
wsk action create $action --kind go:1.18 path/to/$action.zip
```
