# Actions

Building an OpenWhisk action consists in packaging the action's code in a Zip archive.
When written in an interpreted language, the Zip archive may also embed language dependencies;
for example, a virtualenv for Python actions, or `node_modules` folders for NodeJS actions, etc.
Additionally, the action may require a customized Docker image embedding more software pieces: native dependencies, build dependencies, or actually main dependency of a function, for various reasons (most often, package size).

Note that all functions that process an input have a dependency package, because they all depend on a Swift client library; however they may not all need a runtime image.

In the general case, just use the Makefile in the root of this repository to build all or a single action.

## Organization: languages

Actions are grouped by language of implementation;
there may be redundant actions between different languages.

* [`c`](c/): native actions implemented in C
* [`go`](go/): native actions implemented in Go
  * in the case of Go, OpenWhisk's Go runtime is able to compile the action's source code on the fly, but we prefer compiling them beforehand
* [`java`](java/): interpreted actions written in Java 8
  * OpenWhisk's [official Java runtime](https://github.com/apache/openwhisk-runtime-java) is stuck at Java version 8
* [`java11`](java11/): interpreted actions written in Java 11
  * they must be run in an [unofficial update](https://gitlab.com/Thrar/openwhisk-runtime-java-11) of OpenWhisk's Java runtime
* [`javascript`](javascript/): interpreted actions written in NodeJS 14
  * OpenWhisk provides [several NodeJS runtime versions](https://github.com/apache/openwhisk-runtime-nodejs)
* [`python`](python/): interpreted actions written in Python 3

## Organization: dependencies

Inside each language folder, actions are grouped under folders by their main dependency, i.e., the major binary or library that is used to provide the action's service.
This is done because those actions are all built the same way.

As a special case, the `none` dependency/folder gathers actions that do not have such a dependency, i.e., they directly implement their service.
This is the case mostly for very simple functions, or ones that do not process any complex input.

READMEs in the language folders describe those dependencies in tables.
Here is an explanation of the common values found in those tables:

* value "parameters" in "Input kind" means that the actions using this dependency do not fetch anything from the Swift storage backend, in other words, they only process their parameters,
  * they may still process an image, etc., that could be encoded in base64 for example;
* value "yes\*" in "Dependency package" indicates that the dependency package is only required because of the library to communicate with the Swift storage backend;
* column "Image content" gives the reason(s) a custom runtime image is needed:
  * `b`: the action needs build dependencies;
  * `d`: the main dependency of the action must be embedded in the runtime image (instead of the dependency package).
  * `n`: the action needs other native dependencies;
