# Java 11 actions

They must be run in an [unofficial update](https://gitlab.com/Thrar/openwhisk-runtime-java-11) of OpenWhisk's Java runtime.

| Dependency | Input kind | Dependency package | Runtime image | Image content |
|-|-:|-:|-:|-:|
| [`imagej`](imagej/) | image | yes | no | N/A |
| [`none`](none/) | parameters | yes\* | no | N/A |

## Building

Java actions are built and packaged as Jars using the build system gradle.
You can manually build and package a Java action simply by running:

```sh
./gradlew jar
```

Not that contrary to most languages, Java actions are built using gradle on the host.
First, gradle is not available in OpenWhisk's Java action runtime;
second, the Java version and the required dependencies that the action expects to find in this runtime, are set in the action's `build.gradle` file.

## Deploying

Creating an action from a Jar file is recognized by OpenWhisk's CLI as wanting the Java runtime.
However in the case of Java 11, this flavour is probably not the default Java runtime version used by OpenWhisk, so you must specify the kind:

```sh
# Name of the action to deploy.
action=myaction

# Assuming the Java 11 runtime has been deployed and declared in OpenWhisk as `java:11`.
wsk action create $action --kind java:11 path/to/$action.jar
```

This is valid until the action requires a custom Docker runtime, for its dependencies for example (see other languages).
