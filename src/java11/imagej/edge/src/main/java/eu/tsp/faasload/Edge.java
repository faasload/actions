package eu.tsp.faasload;

import ij.ImagePlus;
import ij.process.ImageProcessor;
import org.javaswift.joss.client.factory.AccountFactory;
import org.javaswift.joss.client.factory.AuthenticationMethod;
import org.javaswift.joss.exception.CommandException;
import org.javaswift.joss.model.Account;
import org.javaswift.joss.model.Container;
import org.javaswift.joss.model.StoredObject;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Edge {
    String authUrl;
    String user;
    String key;

    String inContName;
    String outContName;

    String objectName;

    private Account account;

    private final Map<String, Object> response;

    public Edge(Map<String, Object> params) {
        response = new HashMap<>();

        authUrl = (String) params.get("authurl");
        user = (String) params.get("user");
        key = (String) params.get("key");

        inContName = (String) params.get("incont");
        outContName = (String) params.get("outcont");

        objectName = (String) params.get("object");
    }

    public static Map<String, Object> main(Map<String, Object> params, Map<String, Object> owVars) {
        Edge edge = new Edge(params);

        return edge.run();
    }

    public Map<String, Object> run() {
        ImagePlus inImg;

        account = new AccountFactory()
                .setUsername(user)
                .setPassword(key)
                .setAuthUrl(authUrl)
                .setAuthenticationMethod(AuthenticationMethod.BASIC)
                .createAccount();

        try {
            inImg = downloadImage();
        } catch (IOException e) {
            System.err.println(response.get("error_msg"));
            return response;
        }

        ImagePlus outImg = processImage(inImg);

        try {
            uploadImage(outImg);
        } catch (IOException e) {
            System.err.println(response.get("error_msg"));
            return response;
        }

        return response;
    }

    private ImagePlus downloadImage() throws IOException {
        StoredObject imageObj = account.getContainer(inContName).getObject(objectName);


        try (InputStream is = imageObj.downloadObjectAsInputStream()) {
            return new ImagePlus("noname", ImageIO.read(is));
        } catch (IOException e) {
            response.put("error_msg", "failed downloading image \"" + objectName + "\" from container \"" + inContName + "\":\n" + e);
            throw e;
        }
    }

    private ImagePlus processImage(ImagePlus inImg) {
        ImageProcessor imgProc = inImg.getProcessor();
        imgProc.findEdges();

        return new ImagePlus("noname", imgProc);
    }

    private void uploadImage(ImagePlus image) throws IOException {
        byte[] imageBytes;

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            ImageIO.write(image.getBufferedImage(), "jpg", bos);
            imageBytes = bos.toByteArray();
        } catch (IOException e) {
            response.put("error_msg", "failed writing output image to bytes:\n" + e);
            throw e;
        }

        Container outCont = account.getContainer(outContName);
        try {
            outCont.create();
        } catch (CommandException e) {
            if (!(e.getHttpStatusCode() >= 200 && e.getHttpStatusCode() < 300)) {
                response.put("error_msg", "failed creating output container \"" + outContName + "\":\n" + e);
                throw new IOException(e);
            }
        }
        outCont.makePublic();

        StoredObject imageObj = outCont.getObject(UUID.randomUUID() + "_edge_" + objectName);
        imageObj.uploadObject(imageBytes);
    }
}
