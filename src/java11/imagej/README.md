# Java 11 actions: ImageJ

Java actions written around [ImageJ](https://mvnrepository.com/artifact/net.imagej/ij).

* [`blur`](blur/): blur
* [`edge`](edge/): outline edges
* [`resize`](resize/): resize
* [`rotate`](rotate/): rotate

## Building

The dependencies of those Java actions will be handled by gradle and embedded in their Jars;
see [README for Java 11 actions](../README.md).

## Deploying

Those Java 11 actions only require that OpenWhisk's Docker runtime image for Java 11 be used;
See [README for Java 11 actions](../README.md) for full instructions.
