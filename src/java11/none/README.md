# Java 11 actions: none

Java actions written without any main dependency.

* [`autocomplete`](autocomplete/): complete a prefix using a dictionary
* [`gctest`](gctest/): dummy action to trigger the garbage collector
* [`identity`](identity/): dummy action that returns its arguments

## Building

Currently, those Java 11 actions do not require any dependency;
see [README for Java 11 actions](../README.md).

## Deploying

Those Java 11 actions only require that OpenWhisk's Docker runtime image for Java 11 be used;
See [README for Java 11 actions](../README.md) for full instructions.
