package eu.tsp.faasload;

import org.javaswift.joss.client.factory.AccountFactory;
import org.javaswift.joss.client.factory.AuthenticationMethod;
import org.javaswift.joss.model.Account;
import org.javaswift.joss.model.StoredObject;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Identity {
    private final Map<String, Object> response;

    private final Object param;

    public Identity(Map<String, Object> params) {
        response = new HashMap<>();

        param = params.get("parameter");
    }

    public static Map<String, Object> main(Map<String, Object> params, Map<String, Object> owVars) {
        Identity identity = new Identity(params);

        return identity.run();
    }

    public Map<String, Object> run() {
        response.put("result", param);

        return response;
    }
}

