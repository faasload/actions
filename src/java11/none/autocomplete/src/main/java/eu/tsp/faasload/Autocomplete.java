package eu.tsp.faasload;

import org.javaswift.joss.client.factory.AccountFactory;
import org.javaswift.joss.client.factory.AuthenticationMethod;
import org.javaswift.joss.model.Account;
import org.javaswift.joss.model.StoredObject;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Autocomplete {
    public static final String[] NO_COMPLETIONS = {};

    String authUrl;
    String user;
    String key;

    String completionIndexCont;
    String completionIndexName;

    String term;
    Integer maxResults;

    private Account account;

    private String[] completionIndex;

    private final Map<String, Object> response;

    public Autocomplete(Map<String, Object> params) {
        response = new HashMap<>();

        authUrl = (String) params.get("authurl");
        user = (String) params.get("user");
        key = (String) params.get("key");

        completionIndexCont = (String) params.get("indexcont");
        completionIndexName = (String) params.get("index");

        term = (String) params.get("term");
        maxResults = (Integer) params.get("maxres");
        if (maxResults == null)
            maxResults = Integer.MAX_VALUE;
    }

    public static Map<String, Object> main(Map<String, Object> params, Map<String, Object> owVars) {
        Autocomplete autocomplete = new Autocomplete(params);

        return autocomplete.run();
    }

    public Map<String, Object> run() {
        account = new AccountFactory()
                .setUsername(user)
                .setPassword(key)
                .setAuthUrl(authUrl)
                .setAuthenticationMethod(AuthenticationMethod.BASIC)
                .createAccount();

        completionIndex = downloadCompletionIndex();

        String[] completions = autocomplete();

        response.put("results", completions);

        return response;
    }

    private String[] downloadCompletionIndex() {
        StoredObject indexObj = account.getContainer(completionIndexCont).getObject(completionIndexName);

        String rawIndex = new String(indexObj.downloadObject(), StandardCharsets.US_ASCII);

        return rawIndex.split("\\n");
    }

    private String[] autocomplete() {
        Comparator<String> prefixComparator = new PrefixComparator(term.length());

        int indexOfAMatch = Arrays.binarySearch(completionIndex, term, prefixComparator);
        if (indexOfAMatch < 0)
            return NO_COMPLETIONS;

        int indexOfFirstMatch = findStartOfMatches(indexOfAMatch, prefixComparator);

        return takeAllMatchesFrom(indexOfFirstMatch, prefixComparator);
    }

    private int findStartOfMatches(int indexOfAMatch, Comparator<String> comparator) {
        int i = indexOfAMatch;

        while (i > 0 && comparator.compare(completionIndex[i - 1], term) == 0)
            i--;

        return i;
    }

    private String[] takeAllMatchesFrom(int fromIndex, Comparator<String> comparator) {
        int toIndex = fromIndex + 1;

        while (toIndex < completionIndex.length && comparator.compare(completionIndex[toIndex], term) == 0)
            toIndex++;

        // Default value of maxResults is MAX_INT, don't add to it! This explains how we determine the end of range.
        return Arrays.copyOfRange(completionIndex, fromIndex, fromIndex + Math.min(toIndex - fromIndex, maxResults));
    }

    public static class PrefixComparator implements Comparator<String> {
        private final int prefixLength;

        public PrefixComparator(int prefixLength) {
            this.prefixLength = prefixLength;
        }

        @Override
        public int compare(String o1, String o2) {
            return o1.substring(0, Math.min(prefixLength, o1.length()))
                    .compareToIgnoreCase(o2.substring(0, Math.min(prefixLength, o2.length())));
        }
    }
}
