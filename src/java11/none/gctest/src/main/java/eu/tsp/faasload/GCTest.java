package eu.tsp.faasload;

import java.util.HashMap;
import java.util.Map;

/**
 * GCTest: testing garbage collection behavior.
 * Suggested JVM options: -Xms2m -Xmx8m
 * Copyright (c) 2003 by Dr. Herong Yang, herongyang.com
 */
class GCTest {
    static MyList objList = null;
    static int wait = 10; // milliseconds
    static int initSteps = 16; // 2 MB
    static int testSteps = 1;
    static int objSize = 128; // 1/8 MB

    public static Map<String, Object> main(Map<String, Object> params, Map<String, Object> owVars) {
        initSteps = ((Double)params.get("init-steps")).intValue();
        testSteps = ((Double)params.get("test-steps")).intValue();

        objList = new MyList();
        myTest();

        return new HashMap<>();
    }

    public static void myTest() {
        for (int m = 0; m < initSteps; m++) {
            mySleep(wait);
            objList.add(new MyObject());
        }
        for (int n = 0; n < 10 * 8 * 8 / testSteps; n++) {
            for (int m = 0; m < testSteps; m++) {
                mySleep(wait);
                objList.add(new MyObject());
            }
            for (int m = 0; m < testSteps; m++) {
                mySleep(wait);
                objList.removeTail();
            }
        }
    }

    static void mySleep(int t) {
        try {
            Thread.sleep(t);
        } catch (InterruptedException e) {
            System.out.println("Interrupted...");
        }
    }

    static class MyObject {
        private static long count = 0;
        private long[] obj = null;
        public MyObject next = null;
        public MyObject prev = null;

        public MyObject() {
            count++;
            obj = new long[objSize * 128];
        }

        protected final void finalize() {
            count--;
        }

        static long getCount() {
            return count;
        }
    }

    static class MyList {
        static long count = 0;
        MyObject head = null;
        MyObject tail = null;

        void add(MyObject o) {
            // add the new object to the head;
            if (head == null) {
                head = o;
                tail = o;
            } else {
                o.prev = head;
                head.next = o;
                head = o;
            }
            count++;
        }

        void removeTail() {
            if (tail != null) {
                if (tail.next == null) {
                    tail = null;
                    head = null;
                } else {
                    tail = tail.next;
                    tail.prev = null;
                }
                count--;
            }
        }
    }
}
