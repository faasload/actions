# Action in C: `malloc` test

This action reads and writes values in a dynamically allocated memory segment.

The purpose is to test instrumenting calls to memory allocation (like `malloc`) from the "container runtime".
That is to say, dynamically inject code into the container of the action that is called when the user action calls `malloc` and such.
