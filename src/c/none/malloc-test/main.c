#include "cJSON/cJSON.h"
#include "nativewsk/nativewsk.h"

#include <sys/mman.h>

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int _main(size_t size) {
  uint64_t *alloc = NULL;
  uint64_t ret = 1337;
  uint64_t i = 0;
  int my_errno;

  if ((alloc = malloc(size)) == NULL) {
    my_errno = errno;
    output_err("error: failed allocating", my_errno);
    return -my_errno;
  } else {
    printf("info: allocated %#lx bytes, for an array of %lx uint64_t\n", size,
           size / sizeof(*alloc));
  }

  for (i = 0; i < size / sizeof(*alloc); i++)
    alloc[i] = i;

  free(alloc);

  ret = i;

  return ret;
}

int main(int argc, char *argv[]) {
  int res;

  cJSON *args;
  cJSON *res_json, *res_args_json;

  size_t size;
  char *sz_str;

  if (argc < 2) {
    output_err("error: need arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if ((args = cJSON_Parse(argv[1])) == NULL) {
    output_err("error: failed parsing arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if (getarg_as_string(args, "size", &sz_str) < 0) {
    output_err("error: bad value for argument \"size\", expected String",
               EXIT_FAILURE);
    return -1;
  }
  if ((size = strtoul(sz_str, NULL, 16)) == 0) {
    output_err("error: invalid value when converting argument \"size\" to "
               "unsigned long",
               EXIT_FAILURE);
    return -1;
  }
  printf("parsed malloc size from args: %#lx\n", size);

  if ((res = _main(size)) < 0)
    return EXIT_FAILURE;

  res_json = cJSON_CreateObject();
  cJSON_AddNumberToObject(res_json, "res", res);

  res_args_json = cJSON_CreateObject();
  cJSON_AddItemToObject(res_json, "args", res_args_json);
  cJSON_AddStringToObject(res_args_json, "size", sz_str);

  output("success", res_json);

  cJSON_Delete(res_json);

  cJSON_Delete(args);

  return EXIT_SUCCESS;
}
