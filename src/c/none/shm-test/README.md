# Action in C: read-write test in shared memory

This actions touches a value in a shared memory segment.

## Configuration

To actually share the memory segment between multiple actions, **they must share the same IPC namespace** (see [man 7 namespaces](https://man7.org/linux/man-pages/man7/namespaces.7.html)).
It means:

* the containers running the functions must share the same IPC namespace;
* the pods running the functions must share the same IPC namespace;
* OpenWhisk must be modified to create pods and containers that share the same IPC namespace (see ["Customize configuration of action containers"](https://gitlab.com/Thrar/openwhisk/-/blob/master/docs/dev/custom-action-containers.md?ref_type=heads)).

Without this, no action will be able to reuse the shared memory segment, unless you get "lucky" and your invocation reuses the warm instance that already created the memory segment.

## Behavior

This action maps a POSIX shared memory segment named `shm` (must start with a "/") of size `shmsize` (bytes, read as a hexadecimal number).
If the shared memory segment does not exist yet, it will create it;
it will never delete it.

Then, depending on `command` (either `"read"` or `"write"`), it:

* reads the value at `index` (an offset, read as a hexadecimal number, in the shared memory segment taken as an array of `uint64`)
* writes a value (hardcoded) at `index` (same as above)

In all cases, it returns the value at `index`.

## Arguments and result

See [`params.json`](./params.json) for an example of parameters.

The result looks like this:

```json
{
  "res": "0x42",
  "args": {
    ... (the arguments received by the function)
  }
}
```
