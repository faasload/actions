#include "cJSON/cJSON.h"
#include "nativewsk/nativewsk.h"

#include <sys/mman.h>

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define WRITTEN_VALUE 0x2A

enum Command {
  CMD_READ,
  CMD_WRITE,
};

enum Command str2cmd(const char *const cmd_str) {
  if (strcmp(cmd_str, "read") == 0)
    return CMD_READ;
  else if (strcmp(cmd_str, "write") == 0)
    return CMD_WRITE;
  else
    return -1;
}

int strcmd(enum Command cmd, char *str) {
  switch (cmd) {
  case CMD_READ:
    strncpy(str, "read", 5);
    break;
  case CMD_WRITE:
    strncpy(str, "write", 6);
    break;
  default:
    return -1;
  }

  return 0;
}

int _main(const char *shm, size_t shm_size, enum Command cmd, uint64_t index) {
  char *shm_map;
  int shm_fd;
  uint64_t ret = 1337;
  int my_errno;
  uint64_t *target;

  if ((shm_fd = shm_open(shm, O_RDWR, S_IRWXU)) < 0) {
    my_errno = errno;
    if (my_errno == EPERM || my_errno == EACCES) {
      output_err("error: cannot access shared memory, missing rights",
                 my_errno);
      return -my_errno;
    } else if (my_errno != ENOENT) {
      output_err("error: cannot open shared memory", my_errno);
      return -my_errno;
    }

    printf("%s: info: cannot find shared memory \"%s\", creating it\n",
           __func__, shm);

    if ((shm_fd = shm_open(shm, O_RDWR | O_CREAT, S_IRWXU)) < 0) {
      my_errno = errno;
      output_err("error: failed creating shared memory", my_errno);
      return -my_errno;
    } else {
      printf("info: created shared memory \"%s\"\n", shm);
    }

    if (ftruncate(shm_fd, shm_size) < 0) {
      my_errno = errno;
      output_err("error: failed truncating shared memory to size", my_errno);
      return -my_errno;
    } else {
      printf("info: truncated shared memory to %#lx bytes\n", shm_size);
    }
  } else {
    printf("info: found and opened existing shared memory \"%s\"\n", shm);
  }

  if ((shm_map = mmap(NULL, shm_size, PROT_READ | PROT_WRITE, MAP_SHARED,
                      shm_fd, 0)) == MAP_FAILED) {
    my_errno = errno;
    output_err("error: failed mapping shared memory", my_errno);
    return -my_errno;
  }

  if (close(shm_fd) < 0) {
    my_errno = errno;
    output_err("error: failed closing shared memory", my_errno);
    return -my_errno;
  }

  target = &((uint64_t *)shm_map)[index];

  if (cmd == CMD_READ) {
    printf("target byte %#lx = %#lx\n", index, *target);
  } else if (cmd == CMD_WRITE) {
    *target = WRITTEN_VALUE;
    printf("target byte %#lx = %#lx\n", index, *target);
  }

  ret = *target;

  if (munmap(shm_map, shm_size) < 0) {
    my_errno = errno;
    output_err("error: failed unmapping shared memory", my_errno);
    return -my_errno;
  }

  return ret;
}

int main(int argc, char *argv[]) {
  int res;

  cJSON *args;
  cJSON *res_json, *res_args_json;

  size_t shm_size;
  char *shm;
  uint64_t index;
  char *index_str;
  enum Command cmd;
  char *cmd_str;
  char *shm_sz_str;

  if (argc < 2) {
    output_err("error: need arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if ((args = cJSON_Parse(argv[1])) == NULL) {
    output_err("error: failed parsing arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if (getarg_as_string(args, "shmsize", &shm_sz_str) < 0) {
    output_err("error: bad value for argument \"shmsize\", expected String",
               EXIT_FAILURE);
    return -1;
  }
  if ((shm_size = strtoul(shm_sz_str, NULL, 16)) == 0) {
    output_err("error: invalid value when converting argument \"shmsize\" to "
               "unsigned long",
               EXIT_FAILURE);
    return -1;
  }
  printf("parsed shm size from args: %#lx\n", shm_size);

  if (getarg_as_string(args, "shm", &shm) < 0) {
    output_err("error: bad value for argument \"shm\", expected String",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }
  printf("parsed shm from args: %s\n", shm);

  if (getarg_as_string(args, "index", &index_str) < 0) {
    output_err("error: bad value for argument \"index\", expected String",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }
  if ((index = strtoul(index_str, NULL, 16)) == 0) {
    output_err("error: invalid value when converting argument \"index\" to "
               "unsigned long",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }
  printf("parsed index from args: %lx\n", index);

  if (getarg_as_string(args, "command", &cmd_str) < 0) {
    output_err("error: bad value for argument \"command\", expected String",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }
  if ((cmd = str2cmd(cmd_str)) < 0) {
    output_err(
        "error: invalid value when converting argument \"command\" to Command",
        EXIT_FAILURE);
    return EXIT_FAILURE;
  }
  printf("parsed command from args: %s\n", cmd_str);

  if ((res = _main(shm, shm_size, cmd, index)) < 0)
    return EXIT_FAILURE;

  res_json = cJSON_CreateObject();
  cJSON_AddNumberToObject(res_json, "res", res);

  res_args_json = cJSON_CreateObject();
  cJSON_AddItemToObject(res_json, "args", res_args_json);

  cJSON_AddStringToObject(res_args_json, "shmsize", shm_sz_str);
  cJSON_AddStringToObject(res_args_json, "shm", shm);
  cJSON_AddStringToObject(res_args_json, "index", index_str);
  cJSON_AddStringToObject(res_args_json, "command", cmd_str);

  output("success", res_json);

  cJSON_Delete(res_json);

  cJSON_Delete(args);

  return EXIT_SUCCESS;
}
