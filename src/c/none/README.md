# C actions: none
 
Actions written in C, that do not use any major dependency;
they may still use minor dependencies (i.e., that do not define their features), including the Swift storage client.

* [`malloc-test`](malloc-test/): dummy action that touches memory
* [`shm-test`](shm-test/): dummy action that touches POSIX shared memory

## Building

Currently, those C actions do not require any dependency;
see [README for C actions](../README.md).

## Deploying

Those C actions only require that they are specified as native;
See [README for C actions](../README.md) for full instructions.
