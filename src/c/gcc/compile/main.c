#include "cJSON/cJSON.h"
#include "nativewsk/nativewsk.h"

#include <sys/mman.h>
#include <sys/stat.h>

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define COMPILED_OUTPUT_MAX_SZ ((OUTPUT_MAX_SZ + 3) / 4 * 3)

#define SOURCE_ZIP_FN "source.zip"
#define CANONICAL_INPUT_FN "main.c"
#define CANONICAL_OUTPUT_FN "main.o"

int extract_zip_archive(const char *zip, int zip_sz) {
  int my_errno;
  int ret;

  FILE *zip_f;

  if ((zip_f = fopen(SOURCE_ZIP_FN, "w")) == NULL) {
    my_errno = errno;
    output_err("error: failed opening file to write Zip archive", my_errno);
    return -my_errno;
  }

  if ((ret = fwrite(zip, sizeof(char), zip_sz, zip_f)) != zip_sz) {
    my_errno = errno;
    output_err("error: failed writing Zip archive to file", my_errno);
    return -my_errno;
  }

  if ((ret = fclose(zip_f)) == EOF) {
    my_errno = errno;
    output_err("error: failed closing file to write Zip archive", my_errno);
    return -my_errno;
  }

  printf("info: wrote Zip archive to file `" SOURCE_ZIP_FN "`\n");

  if ((ret = system("unzip " SOURCE_ZIP_FN)) < 0) {
    my_errno = errno;
    output_err("error: failed creating a child process to run unzip", my_errno);
    return -my_errno;
  }

  printf("info: executed unzip to extract archive\n");

  if (WIFEXITED(ret)) {
    ret = WEXITSTATUS(ret);
    if (ret != 0) {
      output_err("error: unzip exited in error", ret);
      return -ret;
    }
  } else if (WIFSIGNALED(ret)) {
    ret = WTERMSIG(ret);
    output_err("error: unzip terminated by signal", ret);
    return -ret;
  }

  return ret;
}

int read_gcc_output(char *out) {
  int my_errno;
  int ret;
  FILE *out_f;

  if ((out_f = fopen(CANONICAL_OUTPUT_FN, "rb")) == NULL) {
    my_errno = errno;
    output_err("error: failed opening file to read output", my_errno);
    return -my_errno;
  }

  ret = fread(out, sizeof(char), COMPILED_OUTPUT_MAX_SZ, out_f);
  my_errno = errno;
  if (ferror(out_f)) {
    output_err("error: failed reading output", my_errno);
    return -my_errno;
  } else if (!feof(out_f)) {
    output_err("error: object file too big to be returned as a base64 string",
               EXIT_FAILURE);
    return -EXIT_FAILURE;
  }

  if (fclose(out_f) == EOF) {
    my_errno = errno;
    output_err("error: failed closing file to read output", my_errno);
    return -my_errno;
  }

  return ret;
}

int execute_gcc() {
  int ret;
  int my_errno;

  if ((ret = system("gcc -c " CANONICAL_INPUT_FN)) < 0) {
    my_errno = errno;
    output_err("error: failed creating a child process to run gcc", my_errno);
    return -my_errno;
  }

  printf("info: executed gcc\n");

  if (WIFEXITED(ret)) {
    ret = WEXITSTATUS(ret);
    if (ret != 0) {
      output_err("error: gcc exited in error", ret);
      return -ret;
    }
  } else if (WIFSIGNALED(ret)) {
    ret = WTERMSIG(ret);
    output_err("error: gcc terminated by signal", ret);
    return -ret;
  }

  return ret;
}

int gcc_single_file(char *out, const char *input, int input_sz) {
  int my_errno;
  int ret;

  FILE *input_f;

  if ((input_f = fopen(CANONICAL_INPUT_FN, "w")) == NULL) {
    my_errno = errno;
    output_err("error: failed opening file to write input", my_errno);
    return -my_errno;
  }

  if ((ret = fputs(input, input_f)) == EOF) {
    my_errno = errno;
    output_err("error: failed writing input to file", my_errno);
    return -my_errno;
  }

  if ((ret = fclose(input_f)) == EOF) {
    my_errno = errno;
    output_err("error: failed closing file to write input", my_errno);
    return -my_errno;
  }

  if ((ret = execute_gcc()) < 0)
    return ret;

  if ((ret = read_gcc_output(out)) < 0)
    return ret;

  return ret;
}

int gcc_zip_archive(char *out, const char *input, int input_sz) {
  int ret;

  if ((ret = extract_zip_archive(input, input_sz)) < 0)
    return ret;

  printf("info: extracted Zip archive to disk\n");

  if ((ret = execute_gcc()) < 0)
    return ret;

  if ((ret = read_gcc_output(out)) < 0)
    return ret;

  return ret;
}

int main(int argc, char *argv[]) {
  int ret;
  int my_errno;

  char *act_id;

  cJSON *args;
  cJSON *res_json;

  char *in;
  int in_sz;

  char *out;

  int (*gcc_func)(char *, const char *, int);

  if (argc < 2) {
    output_err("error: need arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if ((act_id = getenv("__OW_ACTIVATION_ID")) == NULL) {
    output_err("error: failed getting OpenWhisk activation ID from environment",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if (mkdir(act_id, S_IRWXU) < 0) {
    my_errno = errno;
    output_err("error: failed creating directory for this request", my_errno);
    return -my_errno;
  }

  if (chdir(act_id) < 0) {
    my_errno = errno;
    output_err("error: failed changing current working directory to directory "
               "for the request",
               my_errno);
    return -my_errno;
  }

  printf("info: set up compilation directory for this request\n");

  if ((args = cJSON_Parse(argv[1])) == NULL) {
    output_err("error: failed parsing arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if (getarg_as_int(args, "sz", &in_sz) < 0) {
    output_err("error: expected size of payload (number of bytes) under \"sz\"",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  // Input is either a Zip archive under key "zip", or a C source file under key
  // "c".
  if (getarg_as_binary(args, "zip", &in) < 0) {
    if (getarg_as_string(args, "c", &in) < 0) {
      output_err("error: expected either a base64-encoded Zip archive under "
                 "\"zip\", or a C source as text under \"c\"; got none",
                 EXIT_FAILURE);
      return EXIT_FAILURE;
    } else {
      printf("info: received C source as text from parameter \"c\"\n");

      gcc_func = gcc_single_file;
    }
  } else {
    printf("info: received base64 Zip archive from parameter \"zip\"\n");

    gcc_func = gcc_zip_archive;
  }

  if ((out = malloc(sizeof(char) * COMPILED_OUTPUT_MAX_SZ)) == NULL) {
    output_err("error: failed allocating memory for output object file",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if ((ret = gcc_func(out, in, in_sz)) < 0)
    return EXIT_FAILURE;

  printf("info: compilation terminated, output size is %dB\n", ret);

  res_json = cJSON_CreateObject();
  cJSON_AddNumberToObject(res_json, "sz", ret);
  cJSON_AddBinaryToObject(res_json, "object_base64", out, ret);

  output("success", res_json);

  cJSON_Delete(res_json);

  free(out);

  cJSON_Delete(args);

  return EXIT_SUCCESS;
}
