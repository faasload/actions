# Action in C: gcc compile

This action compile a single C source file to an object file.
It does not link.

## Parameters

The C source is taken from parameters.
The argument `sz` is always mandatory, and is the number of bytes of the C source passed as argument.
To pass the actual C source, there are two cases:

1. only the C file is required (i.e., no headers): pass the content of the file as a JSON string under key `c`
  * example command to get the correct string: `jq --raw-input --slurp --ascii-output . <SOURCE_FILE`
2. there are header files beside the C source file: pass a Zip archive encoded as a JSON base64-encoded string under key `zip`
  * the C source file **must be named `main.c`**
  * example command to get the correct string: `base64 --wrap=0 ZIP_ARCHIVE`

In all cases, the result of the action (if successful) has two keys under the key `data`:

* `sz`: the size of the output binary object
* `object_base64`: the output binary object, as a JSON base64-encoded string
  * example command to get the output binary object from the action's result JSON object: `jq -r .data.object_base64 | base64 -d > main.o`

## Examples

Here is an example usage for the action (declared as `gcc/compile`) to compile a single C source file:

```sh
c_source_fp="path/to/source.c"

wsk action invoke gcc/compile \
  --param sz "$(wc --chars --total=only "$c_source_fp")" \
  --param c "$(jq --raw-input --slurp --ascii-output . <"$c_source_fp")"
```

And here is an example for the same action, to compile a source file with its headers via a Zip archive:

```sh
zip_source_fp="mycode.zip"

zip "$zip_source_fp" main.c header_A.h header_B.h

wsk action invoke gcc/compile \
  --param sz "$(wc --chars --total=only "$zip_source_fp")" \
  --param zip "$(base64 --wrap=0 "$zip_source_fp")"
```

Finally, here is how you can get the object file from the result:

```sh
# Fetch the last activation's result.
wsk activation result -l | jq -r .data.object_base64 | base64 -d > main.o
```
