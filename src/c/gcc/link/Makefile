BIN = exec
OBJS = main.o
HDRS = $(wildcard *.h)

ACTION = $(notdir $(CURDIR))

# Must be **relative** to the action folder.
VENDORS = ../../_vendors
HOST_INCLUDE = $(VENDORS)/host-include

CC = gcc
DOCKER = docker
ZIP = zip
WSK = wsk -i

CFLAGS = -O0 -g
CFLAGS += -Wall -Werror -pedantic \
	  -Wredundant-decls
CFLAGS += -I$(VENDORS)
CFLAGS += -idirafter $(HOST_INCLUDE) -idirafter $(HOST_INCLUDE)/x86_64-linux-gnu

### DEPENDENCIES
# Prerequisites of the $(BIN) target.
DEPS = $(VENDORS)/cJSON/libcjson.a\
	   $(VENDORS)/nativewsk/nativewsk.a

# Dependencies for the linker.
# Force using static libraries with -l:libname.a.
LDFLAGS = -L$(VENDORS)/cJSON -l:libcjson.a\
		  -L$(VENDORS)/nativewsk -l:libnativewsk.a
### DEPENDENCIES


DOCKER_BUILDER_IMG = openwhisk/dockerskeleton-builder


ifndef INDOCKER		### OUT OF DOCKER BUILDER IMAGE

.PHONY: all builder-image clean

all: $(BIN)

# Note: it depends on the phony target builder-image, that can only be phony because it is the Docker builder image.
# So this recipe always gets executed because its dependency also always is.
# Then inside the Docker builder image, the actual dependency is checked.
$(BIN): builder-image
	$(DOCKER) run --interactive --tty \
		--volume "$(CURDIR):/action/" \
		--volume "$(realpath $(VENDORS)):/action/$(VENDORS)" \
		--volume "/usr/include/:/action/$(HOST_INCLUDE)" \
		--workdir /action/ --user "$$(id -u):$$(id -g)"\
		"$(DOCKER_BUILDER_IMG)" \
		bash -c "make INDOCKER=y"

builder-image:
	@printf "\e[0;94mBuilding Docker builder image %s\e[0m\n" "$(DOCKER_BUILDER_IMG)"
	printf "FROM openwhisk/dockerskeleton:latest\nRUN apk update && apk add gcc libc-dev make" | docker build --tag "$(DOCKER_BUILDER_IMG)" -

clean:
	rm -rf *.o "$(BIN)" "$(ACTION).zip"

else		### IN DOCKER BUILDER IMAGE

.PHONY: all

all: $(BIN)

$(BIN): $(OBJS) $(DEPS)
	$(CC) -o $@ $(OBJS) $(LDFLAGS) 

$(OBJS): $(HDRS)

$(DEPS):
	@printf "\e[0;94mMaking dependency \"%s\"\e[0m\n" "$@"
	$(MAKE) -C "$(dir $@)"

endif

