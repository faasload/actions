# Action in C: gcc link

This action links object files and libraries into an executable.

**Warning:** a limit of this action is that it will link object files against the C library in the runtime container, which is based on Alpine, meaning that the C library linked into the executable is Musl.

## Parameters

All the artifacts are passed as a Zip archive encoded as a JSON base64-encoded string under key `zip`
  * all `.o` files will be linked together
  * libraries may be passed in a `lib` directory in the archive (see below)
  * example command to get the correct string: `base64 --wrap=0 ZIP_ARCHIVE`

The argument `sz` is the number of bytes of the C source passed as argument.

Optionally, link options for gcc may be passed as a JSON string under key `opts`.
This can be used to link against libraries (`-l` flag).

The action will include all subdirectories of an optional `lib` directory in the Zip archive, as search directories for libraries.

In all cases, the result of the action (if successful) has two keys under the key `data`:

* `sz`: the size of the output executable
* `object_base64`: the output executable, as a JSON base64-encoded string
  * example command to get the output executable from the action's result JSON object: `jq -r .data.object_base64 | base64 -d > main`

## Examples

Here is an example usage for the action (declared as `gcc/link`) to link object files and a library into an executable:

```sh
# This example links against lib cJSON, used by this very same action.
zip_source_fp="mycode.zip"

# Just for the example, we are using a static library.
zip "$zip_source_fp" main.o lib/cJSON/libcjson.a

wsk action invoke gcc/link \
  --param sz "$(wc -c --total=only "$zip_source_fp")"
  --param zip "$(base64 --wrap=0 "$zip_source_fp")" \
  --param opts "-l:libcjson.a"
```

Finally, here is how you can get the object file from the result:

```sh
# Fetch the last activation's result.
wsk activation result -l | jq -r .data.object_base64 | base64 -d > main
```
