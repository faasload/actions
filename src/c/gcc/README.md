# C actions: gcc

Actions written in C, that rely on [gcc](https://gcc.gnu.org/) to compile C programs;
they may still use minor dependencies (i.e., that do not define their features), including the Swift storage client.

* [`compile`](compile/): compile C source
* [`link`](link/): link object files and libraries

## Building

gcc cannot be used as a library, so the gcc program itself, as well as the C standard library, are required, meaning a custom Docker runtime image must be built (suggested name: `dockerskeleton:gcc`); see [README for C actions](../README.md).

## Deploying

To deploy a C action that uses gcc, the actual Docker runtime image must be specified because it requires a custom one.
See [README for C actions](../README.md) for full instructions.
