# Action in C: openssl rsa-crypt

This action encrypts or decrypts messages using a DER-encoded RSA key pair.

## Parameters

_Summary:_

* if `encrypt == true`:
  * parameters:
    * `key`: base64-encoded DER-encoded RSA public key
    * `msg`: plain string message
  * result under `data`:
    * `msg_b64`: base64-encoded encrypted message
* if `encrypt == false`:
  * parameters:
    * `key`: base64-encoded DER-encoded RSA private key
    * `msg_b64`: base64-encoded encrypted message
  * result under `data`:
    * `msg`: plain string message

The action determines its task from the parameter under key `encrypt`: if `true`, it will encrypt, if `false`, it will decrypt.

In all cases, it expects the appropriate base64-encoded, DER-encoded RSA key under key `key`:

* if encrypting: the public key
* if decrypting: the private key

In all cases, the message to process is under key `msg`, and the result has a key `data` for the action's result.
However the message's and result's formats vary:

* if encrypting:
  * parameter `msg` is the clear text plain JSON string
  * result `data` key `msg_b64` is the encrypted message as a base64-encoded JSON string
* if decrypting:
  * parameter `msg_b64` is the encrypted message as a base64-encoded JSON string
  * result `data` key `msg` is the clear text plain JSON string

## Examples

Here is an example usage for the action (declared as `openssl/rsa-crypt`) to encrypt a message:

```sh
pub_key="rsa_pub.der"

wsk action invoke openssl/rsa-crypt \
  --param encrypt true --param key "$(base64 --wrap=0 "$pub_key")" \
  --param msg "Secret message" 
```

As a bonus, here is how to get the encrypted message:

```sh
encrypted_msg="encrypted.txt.rsa"

# Fetch the last activation's result.
wsk activation result -l | jq -r .data.msg_b64 | base64 -d > "$encrypted_msg"
```

And here is an example for the same action, to decrypt the message encrypted above:

```sh
priv_key="rsa_priv.der"

wsk action invoke openssl/rsa-crypt \
  --param encrypt false --param key "$(base64 --wrap=0 "$priv_key")" \
  --param msg_b64 "$(base64 --wrap=0 "$encrypted_msg")" 
```

As a bonus, here is how to get the decrypted message:

```sh

```sh
clear_msg="clear.txt"

# Fetch the last activation's result.
wsk activation result -l | jq -r .data.msg > "$clear_msg"
```
