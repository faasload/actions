#include "cJSON/cJSON.h"
#include "nativewsk/nativewsk.h"
#include "openssl/core_names.h"
#include "openssl/decoder.h"
#include "openssl/err.h"
#include "openssl/evp.h"

#include <sys/mman.h>

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/*-
 * Copyright 2021 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the Apache License 2.0 (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */

/*
 * An example that uses EVP_PKEY_encrypt and EVP_PKEY_decrypt methods
 * to encrypt and decrypt data using an RSA keypair.
 * RSA encryption produces different encrypted output each time it is run,
 * hence this is not a known answer test.
 */

static EVP_PKEY *get_key(OSSL_LIB_CTX *libctx, const char *propq,
                         const unsigned char *key, size_t key_len, int public) {
  OSSL_DECODER_CTX *dctx = NULL;
  EVP_PKEY *pkey = NULL;
  int selection = public ? EVP_PKEY_PUBLIC_KEY : EVP_PKEY_KEYPAIR;

  dctx = OSSL_DECODER_CTX_new_for_pkey(&pkey, "DER", NULL, "RSA", selection,
                                       libctx, propq);
  (void)OSSL_DECODER_from_data(dctx, &key, &key_len);

  printf("info: decoded %s RSA DER key\n", public ? "public" : "private");

  OSSL_DECODER_CTX_free(dctx);
  return pkey;
}

/* Set optional parameters for RSA OAEP Padding */
static void set_optional_params(OSSL_PARAM *p, const char *propq) {
  static unsigned char label[] = "label";

  /* "pkcs1" is used by default if the padding mode is not set */
  *p++ = OSSL_PARAM_construct_utf8_string(OSSL_ASYM_CIPHER_PARAM_PAD_MODE,
                                          OSSL_PKEY_RSA_PAD_MODE_OAEP, 0);
  /* No oaep_label is used if this is not set */
  *p++ = OSSL_PARAM_construct_octet_string(OSSL_ASYM_CIPHER_PARAM_OAEP_LABEL,
                                           label, sizeof(label));
  /* "SHA1" is used if this is not set */
  *p++ = OSSL_PARAM_construct_utf8_string(OSSL_ASYM_CIPHER_PARAM_OAEP_DIGEST,
                                          "SHA256", 0);
  /*
   * If a non default property query needs to be specified when fetching the
   * OAEP digest then it needs to be specified here.
   */
  if (propq != NULL)
    *p++ = OSSL_PARAM_construct_utf8_string(
        OSSL_ASYM_CIPHER_PARAM_OAEP_DIGEST_PROPS, (char *)propq, 0);

  /*
   * OSSL_ASYM_CIPHER_PARAM_MGF1_DIGEST and
   * OSSL_ASYM_CIPHER_PARAM_MGF1_DIGEST_PROPS can also be optionally added
   * here if the MGF1 digest differs from the OAEP digest.
   */

  *p = OSSL_PARAM_construct_end();
}

/*
 * The length of the input data that can be encrypted is limited by the
 * RSA key length minus some additional bytes that depends on the padding mode.
 */
static int do_encrypt(OSSL_LIB_CTX *libctx, const unsigned char *key,
                      size_t key_len, const unsigned char *in, size_t in_len,
                      unsigned char *out, size_t *out_len) {
  int ret = 0, public = 1;
  const char *propq = NULL;
  EVP_PKEY_CTX *ctx = NULL;
  EVP_PKEY *pub_key = NULL;
  OSSL_PARAM params[5];

  /* Get public key */
  pub_key = get_key(libctx, propq, key, key_len, public);
  if (pub_key == NULL) {
    ret = -EXIT_FAILURE;
    output_err("error: getting public key failed", ret);
    goto cleanup;
  }
  ctx = EVP_PKEY_CTX_new_from_pkey(libctx, pub_key, propq);
  if (ctx == NULL) {
    ret = -EXIT_FAILURE;
    output_err("error: failed creating encryption context", ret);
    goto cleanup;
  }
  set_optional_params(params, propq);
  /* If no optional parameters are required then NULL can be passed */
  if ((ret = EVP_PKEY_encrypt_init_ex(ctx, params)) <= 0) {
    if (ret == 0)
      ret = -EXIT_FAILURE;
    output_err("error: failed initializing encryption", ret);
    goto cleanup;
  }
  /* Calculate the size required to hold the encrypted data */
  if ((ret = EVP_PKEY_encrypt(ctx, NULL, out_len, in, in_len)) <= 0) {
    if (ret == 0)
      ret = -EXIT_FAILURE;
    output_err("error: failed calculating the size of the buffer to hold "
               "encrypted data",
               ret);
    goto cleanup;
  }
  if (*out_len > BINARY_OUTPUT_MAX_SZ) {
    ret = -EXIT_FAILURE;
    output_err("error: encrypted data is too big to be output as base64", ret);
    goto cleanup;
  }
  if ((ret = EVP_PKEY_encrypt(ctx, out, out_len, in, in_len)) <= 0) {
    if (ret == 0)
      ret = -EXIT_FAILURE;
    output_err("error: failed encrypting", ret);
    goto cleanup;
  }

  printf("info: encrypted message (%luB)\n", *out_len);

  ret = 0;

cleanup:
  EVP_PKEY_free(pub_key);
  EVP_PKEY_CTX_free(ctx);

  return ret;
}

static int do_decrypt(OSSL_LIB_CTX *libctx, const unsigned char *key,
                      size_t key_len, const unsigned char *in, size_t in_len,
                      unsigned char *out, size_t *out_len) {
  int ret = 0, public = 0;
  const char *propq = NULL;
  EVP_PKEY_CTX *ctx = NULL;
  EVP_PKEY *priv_key = NULL;
  OSSL_PARAM params[5];

  /* Get private key */
  priv_key = get_key(libctx, propq, key, key_len, public);
  if (priv_key == NULL) {
    ret = -EXIT_FAILURE;
    output_err("error: failed getting private key", ret);
    goto cleanup;
  }
  ctx = EVP_PKEY_CTX_new_from_pkey(libctx, priv_key, propq);
  if (ctx == NULL) {
    ret = -EXIT_FAILURE;
    output_err("error: failed creating encryption context", ret);
    goto cleanup;
  }

  /* The parameters used for encryption must also be used for decryption */
  set_optional_params(params, propq);
  /* If no optional parameters are required then NULL can be passed */
  if ((ret = EVP_PKEY_decrypt_init_ex(ctx, params)) <= 0) {
    if (ret == 0)
      ret = -EXIT_FAILURE;
    output_err("error: failed initializing decryption", ret);
    goto cleanup;
  }
  /* Calculate the size required to hold the decrypted data */
  if (EVP_PKEY_decrypt(ctx, NULL, out_len, in, in_len) <= 0) {
    if (ret == 0)
      ret = -EXIT_FAILURE;
    output_err("error: failed calculating the size of the buffer to hold "
               "decrypted data",
               ret);
    goto cleanup;
  }
  if (*out_len > OUTPUT_MAX_SZ) {
    ret = -EXIT_FAILURE;
    output_err("error: decrypted message is too big to be output", ret);
    goto cleanup;
  }
  if ((ret = EVP_PKEY_decrypt(ctx, out, out_len, in, in_len)) <= 0) {
    if (ret == 0)
      ret = -EXIT_FAILURE;
    output_err("error: failed decrypting", ret);
    goto cleanup;
  }

  printf("info: decrypted message (%luB)\n", *out_len);

  ret = 0;

cleanup:
  EVP_PKEY_free(priv_key);
  EVP_PKEY_CTX_free(ctx);

  return ret;
}

int _main(bool encrypt, const unsigned char *key, size_t key_len,
          const char *in, size_t in_len, char *out, size_t *out_len) {
  int ret;

  OSSL_LIB_CTX *libctx = NULL;

  if (encrypt) {
    if ((ret = do_encrypt(libctx, key, key_len, (unsigned char *)in, in_len,
                          (unsigned char *)out, out_len)) < 0)
      goto cleanup;

    ret = EXIT_SUCCESS;
  } else {
    if ((ret = do_decrypt(libctx, key, key_len, (unsigned char *)in, in_len,
                          (unsigned char *)out, out_len)) < 0)
      goto cleanup;

    ret = EXIT_SUCCESS;
  }

cleanup:
  OSSL_LIB_CTX_free(libctx);

  return ret;
}

int main(int argc, char *argv[]) {
  int ret;

  cJSON *args;
  cJSON *res_json;

  unsigned char *key;
  char *in, *out;
  size_t key_len, in_len, out_len;

  bool encrypt;

  if (argc < 2) {
    output_err("error: need arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if ((args = cJSON_Parse(argv[1])) == NULL) {
    output_err("error: failed parsing arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if (getarg_as_bool(args, "encrypt", &encrypt) < 0) {
    output_err("error: bad value for argument \"encrypt\", expected Bool",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }
  printf("info: asked to %s by parameters\n", encrypt ? "encrypt" : "decrypt");

  if (encrypt) {
    // This is not actually the input length here, just the return value.
    if ((in_len = getarg_as_string(args, "msg", &in)) >= 0)
      // Include terminating NULL byte.
      in_len = strlen(in) + 1;
  } else {
    in_len = getarg_as_binary(args, "msg_b64", &in);
  }
  if (in_len < 0) {
    output_err("error: bad value for message argument (\"msg\" or \"msg_b64\" "
               "depending on \"encrypt\"), expected String",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }
  printf("info: got %s message from parameters (%luB)\n",
         encrypt ? "clear text" : "encrypted", in_len);
  if (encrypt)
    printf("info: message to encrypt: \"%s\"\n", in);

  if ((key_len = getarg_as_binary(args, "key", (char **)&key)) < 0) {
    output_err("error: bad value for argument \"key\", expected String",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }
  printf("info: got %s DER RSA key from parameters\n",
         encrypt ? "public" : "private");

  out = malloc(encrypt ? sizeof(unsigned char) * BINARY_OUTPUT_MAX_SZ
                       : sizeof(char) * OUTPUT_MAX_SZ);
  if (out == NULL) {
    ret = -EXIT_FAILURE;
    output_err("error: failed allocating memory for output", ret);
    return ret;
  }

  if ((ret = _main(encrypt, key, key_len, in, in_len, out, &out_len)) < 0)
    return EXIT_FAILURE;

  if (!encrypt)
    printf("info: decrypted message: \"%s\"\n", out);

  res_json = cJSON_CreateObject();
  cJSON_AddNumberToObject(res_json, "ret", ret);

  if (encrypt)
    cJSON_AddBinaryToObject(res_json, "msg_b64", out, out_len);
  else
    cJSON_AddStringToObject(res_json, "msg", out);

  output("success", res_json);

  OPENSSL_free(out);

  cJSON_Delete(res_json);

  cJSON_Delete(args);

  return EXIT_SUCCESS;
}
