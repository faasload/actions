# C actions: openssl

Actions written in C, that rely on [openssl](https://www.openssl.org/) to to cryptography;
they may still use minor dependencies (i.e., that do not define their features), including the Swift storage client.

* [`rsa-crypt`](rsa-crypt/): encrypt or decrypt messages

## Building

openssl is statically linked with the action's executable (from its directory in [`../../_vendors`](../../_vendors)).
See [README for C actions](../README.md) for full instructions.

## Deploying

Those C actions only require that they are specified as native;
See [README for C actions](../README.md) for full instructions.
