# C actions: igraph

Actions written in C, that rely on [igraph](https://igraph.org/) to process graphs;
they may still use minor dependencies (i.e., that do not define their features), including the Swift storage client.

* [`bfs`](bfs/): traverses a random graph breadth-first

## Building

igraph is statically linked with the action's executable (from its directory in [`../../_vendors`](../../_vendors)).
See [README for C actions](../README.md) for full instructions.

## Deploying

Those C actions only require that they are specified as native;
See [README for C actions](../README.md) for full instructions.
