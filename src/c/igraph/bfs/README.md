# Action in C: igraph bfs

This actions traverses a randomly-generated graph breadth-first.

It first generates a random graph of the given size using a [Barabási game model](https://igraph.org/c/doc/igraph-Generators.html#igraph_barabasi_game), then it returns the breadth-first list of the vertices of the graph as well as its layers.

## Parameters

The action expects the size of the graph, defined as the number of vertices, under key `sz`.

It returns three arrays under key `data`:

* `visitorder`: the visit order of the vertices, i.e., the list of the vertices of the graph in the order they were visited during the breadth-first search
* `layers`: the first visited vertices of the layers in the graph, as an index in `visitorder`
  * i.e., `layers[i]` gives the index in `visitorder` of the first visited vertex in layer `i`, and the last visited vertex in layer `i` is at index `layers[i+1] - 1` in `visitorder`
* `parents`: the parents of the visited vertices
  * can be `-2` if the vertex was not visited, and `-1` if the vertex is the root

## Examples

Here is an example usage for the action (declared as `igraph/bfs`), to do a breadth-first search on a graph with 100 vertices:

```sh
wsk action invoke --blocking igraph/bfs --param sz 100
```

