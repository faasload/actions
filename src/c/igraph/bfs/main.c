#include "cJSON/cJSON.h"
#include "igraph/igraph.h"
#include "nativewsk/nativewsk.h"

#include <sys/mman.h>

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void igraph_error_handler_openwhisk(const char *reason, const char *file,
                                    int line, igraph_error_t igraph_errno) {
  output_err("error: an igraph function failed, see logs", igraph_errno);
  // Call the "print & ignore" igraph error handler to "free the temporarily
  // allocate memory and print an error message".
  igraph_error_handler_printignore(reason, file, line, igraph_errno);
}

int _main(size_t sz, long int **out_visit_order, long int **out_layers,
          int *out_layers_sz, long int **out_parents) {
  /* The reference for this example is
   * https://github.com/spcl/serverless-benchmarks/blob/bb793aedc00f80dbcca6e735ef1a1872710eb969/benchmarks/500.scientific/503.graph-bfs/python/function.py.
   * They use a parameter for the size (where test = 10, small = 10000, and
   * large = 100000), and the number of outgoing edges m = 10.
   */

  igraph_error_t igraph_ret;
  int my_errno = 0;

  igraph_t graph;
  igraph_vector_int_t visit_order, layers, parents;
  igraph_vector_int_init(&visit_order, 0);
  igraph_vector_int_init(&layers, 0);
  igraph_vector_int_init(&parents, 0);

  // Power of the preferential attachment.
  igraph_real_t power = 1;
  // Number of outgoing edges for each vertex.
  igraph_integer_t m = 10;
  // Algorithm for network generation.
  igraph_barabasi_algorithm_t algo = IGRAPH_BARABASI_PSUMTREE;

  // ID of the root vertex for the BFS search.
  igraph_integer_t search_root = 0;

  igraph_set_error_handler(igraph_error_handler_openwhisk);

  if ((igraph_ret = igraph_barabasi_game(&graph, sz, power, m, NULL, 0, 1.0f, 0,
                                         algo, NULL)) != IGRAPH_SUCCESS)
    return -igraph_ret;

  printf("info: generated a Barabasi graph of %lu vertices (m = %ld)\n", sz, m);

  if ((igraph_ret = igraph_bfs_simple(&graph, search_root, 0, &visit_order,
                                      &layers, &parents)) != IGRAPH_SUCCESS)
    return -igraph_ret;

  if ((*out_visit_order = malloc(
           sizeof(long int) * igraph_vector_int_size(&visit_order))) == NULL) {
    my_errno = errno;
    output_err("error: failed allocating output array of visited vertices",
               my_errno);
    return -my_errno;
  }
  *out_layers_sz = igraph_vector_int_size(&layers);
  if ((*out_layers = malloc(sizeof(long int) * *out_layers_sz)) == NULL) {
    my_errno = errno;
    output_err("error: failed allocating output array of layers", my_errno);
    return -my_errno;
  }
  if ((*out_parents = malloc(sizeof(long int) *
                             igraph_vector_int_size(&parents))) == NULL) {
    my_errno = errno;
    output_err("error: failed allocating output array of parents", my_errno);
    return -my_errno;
  }

  printf("info: searched the graph breadth-first, it has %d layers\n",
         *out_layers_sz);

  igraph_vector_int_copy_to(&visit_order, *out_visit_order);
  igraph_vector_int_copy_to(&layers, *out_layers);
  igraph_vector_int_copy_to(&parents, *out_parents);

  return EXIT_SUCCESS;
}

int main(int argc, char *argv[]) {
  int res;

  cJSON *args;
  cJSON *res_json;
  cJSON *visit_order_json, *layers_json, *parents_json;

  long int *visit_order, *layers, *parents;
  int sz, nb_layers;

  if (argc < 2) {
    output_err("error: need arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if ((args = cJSON_Parse(argv[1])) == NULL) {
    output_err("error: failed parsing arguments", EXIT_FAILURE);
    return EXIT_FAILURE;
  }

  if (getarg_as_int(args, "sz", &sz) < 0) {
    output_err("error: bad value for argument \"sz\", expected Number",
               EXIT_FAILURE);
    return EXIT_FAILURE;
  }
  printf("info: asked to generate a graph with %d vertices\n", sz);

  if ((res = _main(sz, &visit_order, &layers, &nb_layers, &parents)) < 0)
    return EXIT_FAILURE;

  res_json = cJSON_CreateObject();
  cJSON_AddNumberToObject(res_json, "res", res);
  cJSON_AddNumberToObject(res_json, "nb_layers", nb_layers);

  visit_order_json = cJSON_AddArrayToObject(res_json, "visitorder");
  for (int i = 0; i < sz; i++)
    cJSON_AddItemToArray(visit_order_json, cJSON_CreateNumber(visit_order[i]));
  layers_json = cJSON_AddArrayToObject(res_json, "layers");
  for (int i = 0; i < nb_layers; i++)
    cJSON_AddItemToArray(layers_json, cJSON_CreateNumber(layers[i]));
  parents_json = cJSON_AddArrayToObject(res_json, "parents");
  for (int i = 0; i < sz; i++)
    cJSON_AddItemToArray(parents_json, cJSON_CreateNumber(parents[i]));

  output("success", res_json);

  cJSON_Delete(res_json);

  cJSON_Delete(args);

  return EXIT_SUCCESS;
}
