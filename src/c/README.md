# C actions

| Dependency | Input kind | Dependency package | Runtime image | Image content |
|-|-:|-:|-:|-:|
| [`gcc`](gcc/) | params | no | yes | dn |
| [`igraph`](igraph/) | none | no | no | N/A |
| [`none`](none/) | none | no | no | N/A |
| [`openssl`](openssl/) | params | no | no | N/A |

## Building

Here are general instructions to build C actions.
They also cover actions that require native dependencies;
see dependencies' specific READMEs for special requirements.

General variables used in code examples below:

```sh
# Name (and directory) of the action to build.
action=myaction
# Name (and directory) of the main dependency of the action (i.e., where the action is located).
dependency=thedependency
# Custom Docker runtime image for the action.
docker=dockerskeleton:mycustomtag
```

### Statically-linked dependencies

C actions are simply built from their source, as C is a compiled language.
Their dependencies are statically linked to the executable file.

However, due to the complexity of handling dependencies, all in the proper runtime environment (i.e., OpenWhisk's `dockerskeleton` Docker runtime image), most C actions will have their own Makefile.

```sh
# Run from the dependency's directory.
cd $dependency
cd $action
make
```

### Other dependencies: custom Docker runtime image

When a custom Docker runtime image is required for other dependencies (other binaries, etc., for example like [gcc](gcc/) actions), just use the provided Dockerfile:

```sh
# Passing the Dockerfile through stdin to docker build will prevent it from sending all the "build context", i.e., all surrounding files, to the Docker daemon.
cat Dockerfile | docker build --tag $docker -
```

Note that this is only required to do once;
then, the image will be available on the local Docker registry to all actions.
You must make it available to all nodes where OpenWhisk is deployed and will run actions, though.
This can be done by pushing the image to Docker Hub, or simply by building it on all nodes: OpenWhisk will use the local image when you use an explicit tag (i.e., **not `latest`**, because it would force a refresh behavior).

## Deploying

See dependencies' specific READMEs for special requirements.
Below are generic instructions.

To create a C action, it must be specified it is a native action:

```sh
wsk action create $action --native path/to/$action.zip
```

If the action requires a custom Docker runtime image (like [gcc](gcc/) actions), the actual Docker runtime image must be specified instead of the plain "native" kind:

```sh
wsk action create $action --docker $docker path/to/$action.zip
```
