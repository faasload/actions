# Java actions

| Dependency | Input kind | Dependency package | Runtime image | Image content |
|-|-:|-:|-:|-:|
| [`imagej`](imagej/) | parameters | yes | no | N/A |

`imagej` does process images, only they come from the parameters, encoded as base64.

## Building

Java actions are built and packaged as Jars using the build system gradle.
You can manually build and package a Java action simply by running:

```sh
./gradlew jar
```

Not that contrary to most languages, Java actions are built using gradle on the host.
First, gradle is not available in OpenWhisk's Java action runtime;
second, the Java version and the required dependencies that the action expects to find in this runtime, are set in the action's `build.gradle` file.

## Deploying

Creating an action from a Jar file is recognized by OpenWhisk's CLI as wanting the Java runtime.
So there is nothing special to tell on the command line:

```sh
# Name of the action to deploy.
action=myaction

wsk action create $action path/to/$action.jar
```

This is valid until the action requires a custom Docker runtime, for its dependencies for example (see other languages).
