package eu.tsp.faasload;

import com.google.gson.JsonObject;
import ij.ImagePlus;
import ij.process.ImageProcessor;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public class Blur {
    public static JsonObject main(JsonObject args) {
        String img_base64 = args.getAsJsonPrimitive("image").getAsString();
        String format = args.getAsJsonPrimitive("format").getAsString();
        double sigma = args.getAsJsonPrimitive("sigma").getAsDouble();

        JsonObject response = new JsonObject();

        ImagePlus inImg;
        try {
            inImg = decodeToImage(img_base64);
        } catch (IllegalArgumentException | IOException err) {
            System.err.println("failed decoding image from base64 string");
            response.addProperty("error_msg", "failed decoding image from base64 string");
            return response;
        }

        ImageProcessor imgProc = inImg.getProcessor();
        imgProc.blurGaussian(sigma);

        ImagePlus outImg = new ImagePlus("noname", imgProc);

        String outImgString;
        try {
            outImgString = encodeToString(outImg, format);
        } catch (IOException e) {
            System.err.println("failed encoding image to base64 string");
            response.addProperty("error_msg", "failed encoding image to base64 string");
            return response;
        }

        response.addProperty("image", outImgString);

        return response;
    }

    public static ImagePlus decodeToImage(String imgBase64) throws IOException {
        Base64.Decoder dec = Base64.getDecoder();
        byte[] imgBytes = dec.decode(imgBase64);

        ByteArrayInputStream bis = new ByteArrayInputStream(imgBytes);
        BufferedImage imgBuff = ImageIO.read(bis);
        bis.close();

        return new ImagePlus("noname", imgBuff);
    }

    public static String encodeToString(ImagePlus img, String format) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(img.getBufferedImage(), format, bos);
        byte[] imgBytes = bos.toByteArray();
        bos.close();

        Base64.Encoder enc = Base64.getEncoder();
        return enc.encodeToString(imgBytes);
    }
}
