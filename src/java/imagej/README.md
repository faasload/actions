# Java actions: ImageJ

Java actions written around [ImageJ](https://mvnrepository.com/artifact/net.imagej/ij).

* [`blur`](blur/): blur an image

## Building

The dependencies of those Java actions will be handled by gradle and embedded in their Jars;
see [README for Java actions](../README.md).

## Deploying

Those Java actions do not require anything;
See [README for Java actions](../README.md) for full instructions.
