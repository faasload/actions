# Python actions: SoundFile

Actions written in Python, that use the [SoundFile](https://pysoundfile.readthedocs.org) library to process sound samples.

* [`extract`](extract/): extract raw audio
* [`fft`](ftt/): compute Fourier transform

## Building

SoundFile requires libsndfile as a native dependency, meaning a custom custom Docker runtime image must be built (suggested name: `python3action:soundfile`);
see [README for Python actions](../README.md).

## Deploying

To deploy a Python action that uses SoundFile, the actual Docker runtime image must be specified because it requires a custom one.
See [README for Python actions](../README.md) for full instructions.
