# Python actions: none

Actions written in Python, that do not use any major dependency;
they may still use minor dependencies (i.e., that do not define their features), including the Swift storage client.

* [`identity`](identity/): dummy action that returns its arguments
* [`sleep`](sleep/): dummy action that sleeps

## Building

Currently, those Python actions do not require any dependency;
see [README for Python actions](../README.md).

## Deploying

Those Python actions only require that their kind be specified (as `python:3`), because even without any dependency, they are packaged as Zip archives;
See [README for Python actions](../README.md) for full instructions.

