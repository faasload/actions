# Python actions

| Dependency | Input kind | Dependency package | Runtime image | Image content |
|-|-:|-:|-:|-:|
| [`moviepy`](moviepy/) | video | yes\* | yes | nbd |
| [`none`](none/) | params | no | no | N/A |
| [`pydub`](pydub/) | audio | yes | yes | n |
| [`soundfile`](soundfile/) | audio | yes | yes | n |
| [`speechrecognition`](speechrecognition/) | audio | yes\* | yes | bd |
| [`wand`](wand/) | image | yes | yes | n |

## Building

Here are general instructions to build Python actions.
They cover actions that require language dependencies or native dependencies;
see dependencies' specific READMEs for special requirements.

General variables used in code examples below:

```sh
# Name (and directory) of the action to build.
action=myaction
# Name (and directory) of the main dependency of the action (i.e., where the action is located).
dependency=thedependency
# Custom Docker runtime image for the action.
docker=python3action:mycustomtag
```

### Language dependencies: `virtualenv`

In general, Python actions are built by embedding the action source code with the `virtualenv` directory of dependencies into a Zip archive.

The creation of `virtualenv` is done with `virtualenv virtualenv`, and to use the same version that the action will run with, it happens in a Docker container run from OpenWhisk's Python runtime image.
Note that in the general case, all actions in a dependency folder share exactly the same dependencies, so they all get the same `virtualenv`, and it only needs to be built once.

So to build the `virtualenv` of a dependency:

```sh
# Run from the dependency's directory.
cd $dependency
# Build virtualenv dependency directory from the container, in a volume to leave it on the host.
# chown to the host's user to fix permissions on produced directory.
docker run --rm --volume "$PWD:/tmp" openwhisk/python3action bash -c \
  "cd /tmp && \
   virtualenv virtualenv && \
   source virtualenv/bin/activate && \
   pip install -r requirements.txt && \
   chown $(id -u):$(id -g) -R virtualenv"
```

And then to build one action of this dependency:

```sh
cd $dependency
# Creating the archive is a bit convoluted to get the paths right.
zip --quiet --recurse-paths $action.zip virtualenv
zip --recurse-paths --junk-paths $action.zip $action/*
```

### Native dependencies: custom Docker runtime image

When a custom Docker runtime image is required for native dependencies (for example for [Wand](wand/) actions), just use the provided Dockerfile:

```sh
# Passing the Dockerfile through stdin to docker build will prevent it from sending all the "build context", i.e., all surrounding files, to the Docker daemon.
cat Dockerfile | docker build --tag $docker -
```

Note that this is only required to do once;
then, the image will be available on the local Docker registry to all actions.
You must make it available to all nodes where OpenWhisk is deployed and will run actions, though.
This can be done by pushing the image to Docker Hub, or simply by building it on all nodes: OpenWhisk will use the local image when you use an explicit tag (i.e., **not `latest`**, because it would force a refresh behavior).

Note that due to using a custom Docker image, **the kind of the action is now "blackbox"** (this does not need to be specified when deploying the action, see below).

## Deploying

See dependencies' specific READMEs for special requirements.
Below are generic instructions.

In general (like for [none](none/) actions), to create a Python action, the kind must be specified because we use a Zip file which hides the nature of the action's source code.

```sh
wsk action create $action --kind python:3 path/to/$action.zip
```

If the action requires a custom Docker runtime image (like [Wand](wand/) actions), the actual Docker runtime image must be specified because it requires a custom one:

```sh
wsk action create $action --docker $docker path/to/$action.zip
```
