# Python actions: Wand

Actions written in Python, that use the [Wand](https://docs.wand-py.org/en/0.6.3/) library to process images.

* [`blur`](blur/): blur
* [`denoise`](denoise/): apply a denoising filter
* [`edge`](edge/): outline edges
* [`resize`](resize/): resize
* [`rotate`](rotate/): rotate
* [`sepia`](sepia/): apply a sepia coloring filter

## Building

Wand is only a ctypes wrapper to ImageMagick, so this native dependency is required, meaning a custom Docker runtime image must be built (suggested name: `python3action:wand`);
see [README for Python actions](../README.md).

## Deploying

To deploy a Python action that uses Wand, the actual Docker runtime image must be specified because it requires a custom one.
See [README for Python actions](../README.md) for full instructions.

