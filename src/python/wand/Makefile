PYTHON_MAIN := __main__.py

# Name of the virtualenv directory built with the dependency.
# It is better to keep it as a hidden folder because this Makefile gets the list
# of actions to build by listing all folders.
# Note that in the final bundle, the directory *must* be named "virtualenv".
VIRTUALENV := .virtualenv

# Name of the custom Docker container runtime image.
DOCKER_IMAGE := python3action
# Tag of the custom Docker container runtime image
DOCKER_TAG := $(notdir $(CURDIR))

OW_ACTION_PYTHON := openwhisk/python3action

UID := $(shell id -u)
GID := $(shell id -g)

ACTIONS := $(patsubst %/.,%,$(wildcard [^_]*/.))
ZIPS := $(addsuffix .zip,$(ACTIONS))

DOCKER_RUNTIME_TAGFILE := .runtime

CLEAN_ACTIONS := $(addprefix clean-,$(ACTIONS))


.PHONY: all $(ACTIONS) clean $(CLEAN_ACTIONS) clean-runtime clean-virtualenv


all: $(ACTIONS)

# Build the custom Docker runtime image with native dependencies.
$(DOCKER_RUNTIME_TAGFILE): Dockerfile
	# Trick to avoid sending the "context" to the Docker daemon.
	cat Dockerfile | docker build --tag $(DOCKER_IMAGE):$(DOCKER_TAG) -
	@touch "$@"

# Build the Python virtualenv with Python dependencies from PyPI
$(VIRTUALENV): requirements.txt
	docker run --rm \
		--volume "$(CURDIR):/tmp" \
		$(OW_ACTION_PYTHON) \
		bash -c \
		"cd /tmp && \
	    	virtualenv \"$@\" && \
			source \"$@\"/bin/activate && \
			pip install -r \"$<\" && \
			chown $(UID):$(GID) -R \"$@\""

$(ACTIONS): %: %.zip

$(ZIPS): %.zip: %/$(PYTHON_MAIN) | $(DOCKER_RUNTIME_TAGFILE) $(VIRTUALENV)
	# Make a temporary symbolic link to the virtualenv so its get its proper name, hardcoded by Openwhisk: "virtualenv".
	ln --symbolic "$(VIRTUALENV)" virtualenv
	zip --quiet --recurse-paths "$@" virtualenv
	zip --quiet --recurse-paths --junk-paths "$@" "$*"/*
	unlink virtualenv

clean: clean-runtime clean-virtualenv $(CLEAN_ACTIONS)

$(CLEAN_ACTIONS): clean-%:
	rm --force "$*.zip"

clean-runtime:
	docker image rm --force $(DOCKER_IMAGE):$(DOCKER_TAG)
	@rm --force "$(DOCKER_RUNTIME_TAGFILE)"

clean-virtualenv:
	rm --force --recursive "$(VIRTUALENV)"
