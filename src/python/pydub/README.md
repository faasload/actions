# Python actions: PyDub

Actions written in Python, that use the [PyDub](https://pydub.com/) library to process sound samples.

* [`convert`](convert/): convert to another format
* [`effects`](effects/): apply effects

## Building

PyDub requires ffmepg as a native dependency, meaning a custom custom Docker runtime image must be built (suggested name: `python3action:pydub`);
see [README for Python actions](../README.md).

## Deploying

To deploy a Python action that uses PyDub, the actual Docker runtime image must be specified because it requires a custom one.
See [README for Python actions](../README.md) for full instructions.
