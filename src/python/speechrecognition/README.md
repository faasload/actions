# Python actions: Speech Recognition

Actions written in Python, that use the [SpeechRecognition](https://github.com/Uberi/speech_recognition#readme) library to transcribe audio voice to text.

* [`google`](google/): transcribe by requesting Google services
* [`sphinx`](sphinx/): transcribe locally using [CMU Sphinx](https://cmusphinx.github.io/wiki/)

## Building

Speech Recognition is too big a language dependency to be embedded in the action archive, meaning a custom custom Docker runtime image must be built (suggested name: `python3action:speechrecognition`);
see [README for Python actions](../README.md).

## Deploying

To deploy a Python action that uses Speech Recognition, the actual Docker runtime image must be specified because it requires a custom one.
See [README for Python actions](../README.md) for full instructions.
