# Python actions: MoviePy

Actions written in Python, that use the [MoviePy](https://zulko.github.io/moviepy/) library to process video files.

* [`frame`](frame/): extract frame
* [`resize`](resize/): resize
* [`setstart`](setstart/): cut start
* [`setvolume`](setvolume/): set volume
* [`subclip`](subclip/): extract subvideo

## Building

MoviePy requires ffmepg as a native dependency, meaning a custom custom Docker runtime image must be built (suggested name: `python3action:moviepy`);
see [README for Python actions](../README.md).

## Deploying

To deploy a Python action that uses MoviePy, the actual Docker runtime image must be specified because it requires a custom one.
See [README for Python actions](../README.md) for full instructions.
