# NodeJS actions

| Dependency | Input kind | Dependency package | Runtime image | Image content |
|-|-:|-:|-:|-:|
| [`sharp`](sharp/) | image | yes | no | N/A |
| [`tesseract`](tesseract/) | image | yes\* | yes | n |

## Building

Here are general instructions to build Javascript actions.
They cover actions that require language dependencies or native dependencies;
see dependencies' specific READMEs for special requirements.

General variables used in code examples below:

```sh
# Change to your target NodeJS version.
NODEJS_VERSION=14

# Name (and directory) of the action to build.
action=myaction
# Name (and directory) of the main dependency of the action (i.e., where the action is located).
dependency=thedependency
# Custom Docker runtime image for the action.
docker=action-nodejs-v$NODEJS_VERSION:mycustomtag
```

### Language dependencies: `node_modules`

In general, NodeJS actions are built by embedding the action source code with the `node_modules` directory of dependencies into a Zip archive.
The creation of `node_modules` is done with `npm install`, and to use the same version that the action will run with, it happens in a Docker container run from OpenWhisk's NodeJS runtime image.
So to build one action manually looks like:

```sh
# Run from the dependency's directory.
cd $dependency
# Build node_modules dependency directory from the container, in a volume to leave it on the host.
# Set user to fix permissions on produced directory.
docker run --interactive --tty \
  --user $(id -u):$(id -g) \
  --volume "$PWD/$action":/nodejsAction \
  openwhisk/action-nodejs-v$NODEJS_VERSION \
  npm install
# Zip it all.
cd "$action" && zip --quiet --recurse-paths ../"$action.zip" * && cd ..
```

Although all actions have the same dependencies and end up with the same `node_modules` directory, they are built separately because of restrictions imposed by npm, and to keep them as cleanly separate NodeJS packages.

### Native dependencies: custom Docker runtime image

When a custom Docker runtime image is required for native dependencies (for example for [Tesseract](tesseract/) actions), just use the provided Dockerfile:

```sh
# Passing the Dockerfile through stdin to docker build will prevent it from sending all the "build context", i.e., all surrounding files, to the Docker daemon.
cat Dockerfile | docker build --tag $docker -
```

Note that this is only required to do once;
then, the image will be available on the local Docker registry to all actions.
You must make it available to all nodes where OpenWhisk is deployed and will run actions, though.
This can be done by pushing the image to Docker Hub, or simply by building it on all nodes: OpenWhisk will use the local image when you use an explicit tag (i.e., **not `latest`**, because it would force a refresh behavior).

Note that due to using a custom Docker image, **the kind of the action is now "blackbox"** (this does not need to be specified when deploying the action, see below).

## Deploying

See dependencies' specific READMEs for special requirements.
Below are generic instructions.

In general (like for [Sharp](sharp/) actions), to create a NodeJS action, the kind must be specified because we use a Zip file which hides the nature of the action's source code.

```sh
wsk action create $action --kind nodejs:$NODEJS_VERSION path/to/$action.zip
```

If the action requires a custom Docker runtime image (like [Tesseract](tesseract/) actions), the actual Docker runtime image must be specified because it requires a custom one:

```sh
wsk action create $action --docker $docker path/to/$action.zip
```
