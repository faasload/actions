# NodeJS actions: Sharp

Actions written in NodeJS, that use the [Sharp](https://sharp.pixelplumbing.com/) library to process images.

* [`blur`](blur/): blur
* [`convert`](convert/): convert to another format
* [`resize`](resize/): resize
* [`sepia`](sepia/): apply a sepia coloring filter

## Building

The general building steps apply;
see [README for Javascript actions](../README.md).

## Deploying

To deploy a NodeJS action that uses Sharp, only the kind must be specified on the command line.
See [README for Javascript actions](../README.md) for full instructions.
