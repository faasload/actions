/*
 Copyright (c) 2019 Princeton University

 This source code is licensed under the MIT license found in the
 LICENSE file in the root directory of this source tree.
 */

const { performance } = require('perf_hooks');
const SwiftClient = require('openstack-swift-client');
const stream = require('stream');
const tesseract = require('node-tesseract-ocr');

async function handler(resolve, reject, args, swiftclient, inputcont, outputcont) {
    let t0 = performance.now();

    var writableToBuffer = new stream.Writable();
    writableToBuffer.data = [];
    writableToBuffer._write = function (chunk, encoding, callback) {
        this.data.push(chunk);
        callback();
    };
    writableToBuffer._final = function (callback) {
        this.databuff = Buffer.concat(this.data);
        callback();
    };

    inputcont.get(args.object, writableToBuffer)
        .then(() => {
            let inputimg = writableToBuffer.databuff;

            let te = performance.now();

            tesseract.recognize(inputimg)
                .then((text) => {
                    let tt = performance.now();

                    var textstream = new stream.Readable();
                    textstream.push(text);
                    textstream.push(null);
                    outputcont.create('ocrimg_' + args.object + '.txt', textstream)
                        .then(() => {
                            let tl = performance.now();

                            let outputsize = Buffer.byteLength(text);

                            resolve({
                                outputsize: outputsize,
                                start_ms: t0,
                                times: {
                                    extract: te - t0,
                                    transform: tt - te,
                                    load: tl - tt
                                }
                            });
                        });
                });
        });      
}

function main(params) {
    let swiftclient = new SwiftClient(new SwiftClient.SwiftAuthenticator(
        params.authurl, params.user, params.key));
    let inputcont = swiftclient.container(params.incont);
    swiftclient.create(params.outcont);
    let outputcont = swiftclient.container(params.outcont);

    return new Promise((resolve, reject) => {
        handler(resolve, reject, params, swiftclient, inputcont, outputcont);
    });
}

exports.main = main;
