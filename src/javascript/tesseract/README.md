# NodeJS actions: Tesseract

Actions written in NodeJS, that use a [NodeJS wrapper](https://github.com/zapolnoch/node-tesseract-ocr) around the [Tesseract](https://tesseract-ocr.github.io/) OCR engine.

* [`ocr-img`](ocr-img/): recognize text from image

## Building

A custom Docker runtime image is required for the native dependency on Tesseract (suggested name: `action-nodejs-v14:tesseract`);
see [README for Javascript actions](../README.md).

## Deploying

To deploy a NodeJS action that uses Tesseract, the actual Docker runtime image must be specified because it requires a custom one.
See [README for Javascript actions](../README.md) for full instructions.
