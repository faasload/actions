# Targets

## Build

* all: build all actions _(default target)_
* `LANG`: build all actions in language `LANG`
* `LANG`/`DEP`: build all actions that use the main dependency `DEP` in language `LANG`
* `LANG`/`DEP`/`ACTION`: build action `ACTION` that uses the main dependency `DEP` in language `LANG`

## List

* list-languages: list all languages that have actions
* list-`LANG`-dependencies: list all dependencies in language `LANG` that have actions
* list-`LANG`/`DEP`-actions: list all actions with dependency `DEP` in language `LANG`

## Clean

* clean: clean everything (all actions, including their dependency artifacts)
* clean-`LANG`: clean all actions in language `LANG`
* clean-`LANG`/`DEP`: clean all actions that use the main dependency `DEP` in language `LANG`
* clean-`LANG`/`DEP`/`ACTION`: clean action `ACTION` that uses the main dependency `DEP` in language `LANG`

## Deploy

* deploy: deploy all actions in the current namespace of OpenWhisk
  * follow the instructions in `manifest.yaml`
